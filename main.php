<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/style.css">
    <title>Resolução da equação</title>
</head>

<body>
    <div class="container" id="saida">
        <h3>Resolução da equação</h3>
        <?php
        $a = isset($_GET["a"]) ? $_GET["a"] : 1; //o valor de a não pode ser núlo para ser uma equação do segundo grau
        $b = isset($_GET["b"]) ? $_GET["b"] : 0;
        $c = isset($_GET["c"]) ? $_GET["c"] : 0;
        echo "a = $a<br/>";
        echo "b = $b<br/>";
        echo "c = $c<br/>";

        $delta = $b * $b - 4 * $a * $c;
        $xv = -$b / 2 * $a;
        $yv = -$delta / 4 * $a;

        echo "delta = $delta<br/>";
        echo "xv = $xv<br/>";
        echo "yv = $yv<br/>";

        if ($delta > 0) {
            echo "A equação possui duas raízes distintas<br/>";
            $x1 = (-$b - sqrt($delta)) / 2 * $a;
            $x2 = (-$b + sqrt($delta)) / 2 * $a;
            echo "x1 = $x1<br/>";
            echo "x2 = $x2<br/>";
        } elseif ($delta == 0) {
            echo "A equação possui duas raízes iguais<br/>";
            echo "x1 = x2 = $xv<br/>";
        } else {
            echo "A equação possui duas raízes complexas<br/>";
            $imag = sqrt(-$delta) / 2 * $a;
            if ($b != 0) {
                echo "x1 = $xv + $imag i<br/>";
                echo "x2 = $xv - $imag i<br/>";
            } else {
                echo "x1 = + $imag i<br/>";
                echo "x2 = - $imag i<br/>";
            }
        }

        ?>
        <br />
        <a href="./index.html" class="btn btn-warning">Voltar</a>
    </div>
</body>

</html>